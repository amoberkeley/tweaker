#! C:\Anaconda2\envs\py37\python.exe

import os
import time
from datetime import datetime
import numpy as np
from numpy import array # needed in order to parse exp_input.txt with eval()
import subprocess # needed in order to call M-LOOP
import configparser # used to import list of Cicero variables
import json # used to parse lists from config file

base_dir = os.path.join('E:\\', 'Machine Learning')
exp_input = 'exp_input.txt'
exp_config = 'exp_config.txt'
exp_config_base = 'exp_config_base.txt'
cicero_overrides = 'cicero_overrides.txt'
variable_settings = 'variable_settings.ini'

config = configparser.ConfigParser()
config.optionxform = str
config.read(variable_settings)

if not 'VARIABLES' in config:
	raise(NameError, 'variables')
	
variables = config['VARIABLES']

if not 'SETTINGS' in config:
	raise(NameError, 'settings')
	
settings = config['SETTINGS'] # TODO: deal with default settings in a cleaner way

try:
	exp_config_base = settings['base_config_file']
except KeyError:
	pass # use the value defined above
	
try:
	exp_config = settings['config_file']
except KeyError:
	pass # use the value defined above
	
try:
	exp_input = settings['exp_input_file']
except KeyError:
	pass # use the value defined above
	
try:
	cicero_overrides = settings['cicero_overrides_file']
except KeyError:
	pass # use the value defined above
	
try:
	t_timeout_max = float(settings.get('t_timeout_max'))
except KeyError:
	t_timeout_max = 180 # seconds, roughly

num_params = len(variables)
param_names = list(variables)

variable_configs = np.array([json.loads(p) for p in variables.values()])

min_boundary = list(variable_configs.T[0])
max_boundary = list(variable_configs.T[1])
first_params = list(variable_configs.T[2])
initial_simplex_displacements = list(variable_configs.T[3])

exp_input_path = os.path.join(base_dir, exp_input)
exp_config_path_common = os.path.join(base_dir, exp_config_base)
exp_config_path = os.path.join(base_dir, exp_config)
cicero_overrides_path = os.path.join(base_dir, cicero_overrides)

os.system(f'copy /y "{exp_config_path_common:s}" "{exp_config_path:s}"')

with open(exp_config_path, 'a') as f_exp_config:
    f_exp_config.write('\n\n# Run-specific parameters\n')
    f_exp_config.write(f'num_params = {num_params:d}\n')
    f_exp_config.write(f'min_boundary = {min_boundary}\n')
    f_exp_config.write(f'max_boundary = {max_boundary}\n')
    f_exp_config.write(f'param_names = {param_names}\n')
    f_exp_config.write(f'initial_simplex_corner = {first_params}\n')
    f_exp_config.write(f'initial_simplex_displacements = {initial_simplex_displacements}\n')

print('Entering loop. Ctrl-C to exit when M-LOOP has finished.') # TODO: watch for final M-LOOP analytics file?

try:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    mloop = subprocess.Popen('M-LOOP')
    time.sleep(5)

    t_update_previous = 0
    t_timeout = 0

    while t_timeout < t_timeout_max:
        try:
            t_update = os.stat(exp_input_path).st_mtime
        
        except FileNotFoundError:
            input('Warning: exp_input file not found. Ensure M-LOOP is running, then hit Enter to continue.')
            continue

        if t_update != t_update_previous:
            t_update_previous = t_update
            t_timeout = 0

            with open(exp_input_path, 'r') as f_exp_input:
                for line in f_exp_input:
                    exec(line)

            if not 'params' in locals():
                raise(NameError, 'params')

            if len(params) != num_params:
                # Fails on the first loop after changing the number of active Cicero variables
                continue

            with open(cicero_overrides_path, 'w') as f_cicero_overrides:
                for i_line in range(num_params):
                    var_name = param_names[i_line]
                    f_cicero_overrides.write(f'{var_name} = {params[i_line]}\n')

            print()

        else:
            t_timeout += 1

        time.sleep(1)

    print('No recent updates. Exiting loop.')

except KeyboardInterrupt:
    print('\nReceived interrupt.')

mloop.terminate()
outs, errs = mloop.communicate()

print(f'\nTerminated M-LOOP.\nOutput:\n{outs}\n\nErrors:\n{errs}\n')
